---
layout: post
section: noticias
---

La primera versión de la web ya está disponible y poco a poco iremos añadiendo contenidos.

<strong>es<span class="red">Libre</span></strong> <span lang="en">is coming!</span>.

Ya está el programa de <strong>es<span class="red">Libre</span></strong> 2019 disponible en la web!
<ul>
<li><a href="{{ '/2019/programa/' | prepend: site.baseurl }}" target="_blank">https://eslib.re/2019/programa/</a></li>
</ul>

Finalmente la programación se compone de charlas en 7 salas temáticas:

- Track 1: Dev + Web
- Track 2: Social
- Track 3: Big Data, Machine Learning and Cloud
- Devroom 1: <a href="{{ '/2019/programa/devrooms/sl_universidad.html' | prepend: site.baseurl }}" target="_blank">Software Libre en la universidad</a>
- Devroom 2: <a href="{{ '/2019/programa/devrooms/privacidad.html' | prepend: site.baseurl }}" target="_blank">Privacidad, descentralización y soberanía digital</a>
- Devroom 3: <a href="{{ '/2019/programa/devrooms/perl.html' | prepend: site.baseurl }}" target="_blank">Perl</a>
- Devroom 4: <a href="{{ '/2019/programa/devrooms/inf_mat.html' | prepend: site.baseurl }}" target="_blank">Informática y matemáticas</a>
